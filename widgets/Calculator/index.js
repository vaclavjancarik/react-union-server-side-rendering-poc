export default {
  name: 'calculator',
  initialize: (store, namespaces) => {
    const containerImport = import('./containers/Root');

    return new Promise(resolve => {
      containerImport.then(container => resolve(container.default));
    });
  },
};
