import { boot } from '../../union-node'; // would be replaced by @lnd/union-bootstrap
import widgets from './widgets';

boot(widgets);

if (module.hot) {
  module.hot.accept('./widgets', () => boot(require('./widgets')));
}
