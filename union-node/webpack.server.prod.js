// would be contained inside the @lnd/union-node module
const fs = require('fs');
const path = require('path');
const webpack = require('webpack');

const config = JSON.parse(fs.readFileSync(path.join(process.cwd(), './config.json')));

const app = 'Liferay'; // would be passed as a command-line argument from union-cli

// http://jlongster.com/Backend-Apps-with-Webpack--Part-I
const getExternals = () => {
  const nodeModules = fs.readdirSync(path.join(process.cwd(), 'node_modules'));
  return nodeModules.reduce((ext, mod) => {
    ext[mod] = 'commonjs ' + mod;
    return ext;
  }, {});
};

module.exports = {
  context: process.cwd(),
  devtool: 'inline-source-map',
  entry: config[app].node.entry,
  externals: getExternals(),
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'stage-0', 'react'],
          },
        },
      },
    ],
  },
  output: {
    path: path.join(process.cwd(), './build'),
    filename: app + '.js',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.PORT': config[app].node.port
    }),
  ],
  target: 'node',
};
