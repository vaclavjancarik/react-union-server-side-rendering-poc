// Express
import express from 'express';
import bodyParser from 'body-parser';

// React
import React from 'React';
import { renderToString } from 'react-dom/server';

// Parsing
import cheerio from 'cheerio';
import R from 'ramda';

// Union
export const boot = widgets => {
  const app = express();

  app.use(bodyParser.json({limit: '50mb'}));

  // change to POST
  app.post('/', (req, res) => {
    // you should POST a JSON, with key `content` specifying the HTML
    const { content } = req.body;
    // const content = `<!doctype html> <html lang="en"> <head> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" /> <title><%= htmlWebpackPlugin.options.title %></title> </head> <body> <!-- This script adds the Roboto font to our project. For more detail go to this site: http://www.google.com/fonts#UsePlace:use/Collection:Roboto:400,300,500 --> <script> var WebFontConfig = { google: { families: [ 'Open Sans:400,700:latin' ] } }; (function() { var wf = document.createElement('script'); wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js'; wf.type = 'text/javascript'; wf.async = 'true'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wf, s); })(); </script> <div id="container"></div> <div className="mainpromo"> <script data-union-app="" type="application/json"> { "name": "calculator", "namespace": "ns", "container": "container", "initialData": { "loanAmount": { "min": 20000, "max": 800000, "step": 1000, "initial": 100000 }, "term": { "limits": [[75000,24,72],[99000,24,84],[199000,24,96],[299000,24,108],[800000,24,120]], "initial": 96 }, "cpi": { "noneValue": 1, "extraValue": 9 }, "url": { "calc": "http://localhost:8090/mmb/cz-staging/lp-expres-pujcka?p_p_id=GeodLoanCalculatorReact_WAR_geodexpressloancalculatorportlet_INSTANCE_DbcRB4SmfUwL&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=calcLoan&p_p_cacheability=cacheLevelPage&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=10", "intermediate": "https://online.moneta.cz/pujcka", "w2b": "/kontaktujte-nas/schuzka-na-pobocce/expres-pujcka", "conditions": "URL pro odkaz Předpoklady výpočtu a závaznost informací" }, "cta": { "color": "#3fb360", "intermediateLabel": "Zjistit více", "w2bLabel": "Pokračovat", "threshold": 200000 }, "productInfo": { "discountType": 1, "productCode": 5005001, "salesProductCode": 50001 }, "affiliate": { "comment": "nevím, co s tím zatím" }, "localization": { "cs": { "modules.calculator.loanAmount": "Message z back-endu" } } } } </script> </div> </body> </html>`;
    const $ = cheerio.load(content);

    const $domConfigs = $('[data-union-app]');
    const domConfigs = [];

    $domConfigs.each(function() {
      domConfigs.push(JSON.parse($(this).html()));
    });

    const domConfigsByName = R.groupBy(R.prop('name'))(domConfigs);
    const widgetsByName = R.indexBy(R.prop('name'))(widgets);

    const promises = [];

    R.mapObjIndexed((domConfigs, widgetName) => {
      const widget = widgetsByName[widgetName];
      const namespaces = R.map(R.prop('namespace'))(domConfigs);

      promises.push(
        widget.initialize().then(RootComponent => {
          domConfigs.map(domConfig => {
            $(`#${domConfig.container}`).html(
              renderToString(<RootComponent />),
            );
          });
        }),
      );
    })(domConfigsByName);

    Promise.all(promises).then(() => {
      res.status(200).send($.html());
    });
  });

  app.listen(process.env.PORT || 3000, error => {
    if (error) {
      console.error(error);
    } else {
      console.log(`Listening on port ${process.env.PORT || 3000}.`);
    }
  });
};
